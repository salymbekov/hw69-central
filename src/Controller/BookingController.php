<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\BookingObject;
use App\Entity\Cottage;
use App\Entity\Pension;
use App\Repository\BookingObjectRepository;
use App\Repository\BookingRepository;
use App\Repository\ClientRepository;
use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BookingController extends Controller
{
    /**
     * @Route("/booking-object-register/{passport}", name="app-booking-object-register")
     * @param Request $request
     * @param ObjectManager $objectManager
     * @param string $passport
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function registerAction(
        Request $request,
        ObjectManager $objectManager,
        string $passport,
        ClientRepository $clientRepository
    ) {
        $main = $request->request->get('main');
        $dop = $request->request->get('dop');
        $user = $clientRepository->findOneByPassportOrEmail($passport, '');
        if ($main['objectType'] === 'cottage') {
            $cottage = new Cottage();
            $this->registerObject($main, $user, $cottage);
            if (in_array('fireplace', $dop['services'])) {
                $cottage->setFireplace(true);
            }
            if (in_array('maid', $dop['services'])) {
                $cottage->setMaid(true);
            }
            $objectManager->persist($cottage);
            $objectManager->flush();
        } else {
            $pension = new Pension();
            $this->registerObject($main, $user, $pension);
            if (in_array('food', $dop['services'])) {
                $pension->setFood(true);
            }
            if (in_array('bar', $dop['services'])) {
                $pension->setMiniBar(true);
            }
            $objectManager->persist($pension);
            $objectManager->flush();
        }
        return new JsonResponse(['result' => $dop]);
    }

    /**
     *
     * @param $main
     * @param $user
     *
     */
    public function registerObject($main, $user, $object)
    {
        $object
            ->setObjectName($main['name'])
            ->setRoomNumber($main['roomNumber'])
            ->setAddress($main['address'])
            ->setPrice($main['price'])
            ->setContactPerson($main['contactPerson'])
            ->setContactPhone($main['contactPhone'])
            ->setOwner($user);
    }

    /**
     * @Route("/get-booking-object/", name="app-get-booking-object")
     * @param BookingObjectRepository $objectRepository
     * @return JsonResponse
     */
    public function getObjects()
    {
        $result = [];
        $objects = $this->getDoctrine()->getRepository(BookingObject::class)->findAll();
        foreach ($objects as $object) {
            $result[] = $object->toArray();
        }
        return new JsonResponse(['result' => $result]);
    }

    /**
     * @Route("/get-concrete-booking-object/", name="app-get-concrete-booking-object")
     * @param Request $request
     * @param BookingObjectRepository $objectRepository
     * @return JsonResponse
     */
    public function getConcreteObjects(
        Request $request,
        BookingObjectRepository $objectRepository
    ) {
        $name = $request->query->get('search');
        $type = $request->query->get('type');
        $min = $request->query->get('min');
        $max = $request->query->get('max');
        $objects = $objectRepository->findByParams($name, $type, $min, $max);
        $result = [];
        foreach ($objects as $object) {
            $result[] = $object->toArray();
        }
        return new JsonResponse(['result' => $result]);
    }

    /**
     * @Route("/get-one-booking-object/", name="app-get-one-booking-object")
     * @param Request $request
     * @param BookingObjectRepository $objectRepository
     * @param BookingRepository $bookingRepository
     * @return JsonResponse
     */
    public function getOneObjectAndCheckBooking(
        Request $request,
        BookingObjectRepository $objectRepository,
        BookingRepository $bookingRepository
    ) {
        $name = $request->query->get('search');
        $object = $objectRepository->findByName($name);
        $result = $object->toArray();
        $bookings = $bookingRepository->findByObjectName($name);
        $booked = [];
        if (!empty($bookings)) {
            foreach ($bookings as $booking) {
                $booked[] = [
                    'number' => $booking->getRoomNumber(),
                    'date' => $booking->getDateOfFreedom()
                ];
            }
        }
        return new JsonResponse(['result' => [$result], 'booked' => $booked]);
    }

    /**
     * @Route("/book-object/", name="app-book-object")
     * @param Request $request
     * @param BookingObjectRepository $objectRepository
     * @param BookingRepository $bookingRepository
     * @return JsonResponse
     */
    public function bookObject(
        Request $request,
        BookingObjectRepository $objectRepository,
        BookingRepository $bookingRepository,
        ObjectManager $objectManager
    ) {
        $name = $request->request->get('objectName');
        $room = $request->request->get('room');
        $tenant = $request->request->get('tenant');
        $bookings = $bookingRepository->findByObjectNameAndRoom($name, $room);
        if(empty($bookings)){
            $date = new DateTime();
            $date->modify('+7 day');
            $booking = new Booking();
            $booking->setRoomNumber($room);
            $booking->setObjectName($name);
            $booking->setTenant($tenant);
            $booking->setDateOfFreedom($date);
            $objectManager->persist($booking);
            $objectManager->flush();
            $result= 'успех';
        }else{
            $result= 'уже забронирован';
        }

        return new JsonResponse(['result' => $result]);
    }

}
