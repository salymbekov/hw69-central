<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Symfony\Component\Security\Core\Clent\ClentInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $passport;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $registeredAt;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $vkontakte;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $google;

    /**
     * One Product has Many Features.
     * @OneToMany(targetEntity="BookingObject", mappedBy="owner")
     */
    private $bookingObject;

    public function __construct()
    {
        $this->registeredAt = new \DateTime("now");
        $this->bookingObject = new ArrayCollection();
    }

    /**
     * @return \DateTime
     */
    public function getRegisteredAt(): ?\DateTime
    {
        return $this->registeredAt;
    }

    /**
     * @param \DateTime $registeredAt
     */
    public function setRegisteredAt(\DateTime $registeredAt): void
    {
        $this->registeredAt = $registeredAt;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return null;
    }

    /**
     * @param mixed $password
     * @return Client
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param mixed $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $passport
     * @return Client
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassport()
    {
        return $this->passport;
    }

    public function __toArray() {
        return [
            'email' => $this->email,
            'password' => $this->password,
            'passport' => $this->passport,
            'vkontakte' => $this->vkontakte,
            'facebook' => $this->facebook,
            'google' => $this->google,
        ];
    }

    /**
     * @param mixed $vkontakte
     * @return Client
     */
    public function setVkontakte($vkontakte)
    {
        $this->vkontakte = $vkontakte;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVkontakte()
    {
        return $this->vkontakte;
    }

    /**
     * @param mixed $facebook
     * @return Client
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param mixed $google
     * @return Client
     */
    public function setGoogle($google)
    {
        $this->google = $google;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGoogle()
    {
        return $this->google;
    }

    /**
     * @return mixed
     */
    public function getBookingObject()
    {
        return $this->bookingObject;
    }

    /**
     * @param mixed $bookingObject
     */
    public function addBookingObject($bookingObject)
    {
        $this->bookingObject->add($bookingObject);
    }
}
