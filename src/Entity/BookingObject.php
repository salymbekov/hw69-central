<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\ManyToOne;


/**
 * @ORM\Entity
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discriminator", type="string")
 * @DiscriminatorMap(
 *     {
 *          "BookingObject" = "BookingObject",
 *          "Pension" = "Pension",
 *          "Cottage" = "Cottage"
 *     }
 * )
 */

class BookingObject
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $objectName;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $objectType;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $roomNumber;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $contactPerson;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $contactPhone;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $price;
    /**
     *
     * @ManyToOne(targetEntity="Client", inversedBy="bookingObject")
     */
    private $owner;

    /**
     * @param mixed $objectName
     * @return BookingObject
     */
    public function setObjectName($objectName)
    {
        $this->objectName = $objectName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getObjectName()
    {
        return $this->objectName;
    }

    /**
     * @param mixed $roomNumber
     * @return BookingObject
     */
    public function setRoomNumber($roomNumber)
    {
        $this->roomNumber = $roomNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoomNumber()
    {
        return $this->roomNumber;
    }

    /**
     * @param mixed $contactPerson
     * @return BookingObject
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param mixed $contactPhone
     * @return BookingObject
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * @param mixed $address
     * @return BookingObject
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $price
     * @return BookingObject
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $owner
     * @return BookingObject
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    public function __toArray() {
        return [
            'objectName' => $this->objectName,
            'roomNumber' => $this->roomNumber,
            'contactPerson' => $this->contactPerson,
            'contactPhone' => $this->contactPhone,
            'address' => $this->address,
            'price' => $this->price,
            'type' => $this->objectType,
        ];
    }

    /**
     * @return mixed
     */
    public function getObjectType()
    {
        return $this->objectType;
    }

    /**
     * @param mixed $objectType
     * @return BookingObject
     */
    public function setObjectType($objectType)
    {
        $this->objectType = $objectType;
        return $this;
    }
}