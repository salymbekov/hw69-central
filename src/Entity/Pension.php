<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity(repositoryClass="App\Repository\PensionRepository")
 * Class Pension
 * @package App\Entity
 */
class Pension extends BookingObject
{
    /**
     * @ORM\Column(type="boolean", length=254, nullable=true)
     */
    private $food;

    /**
     * @ORM\Column(type="boolean", length=254, nullable=true)
     */
    private $miniBar;

    /**
     * @param mixed $food
     * @return Pension
     */
    public function setFood($food)
    {
        $this->food = $food;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * @param mixed $miniBar
     * @return Pension
     */
    public function setMiniBar($miniBar)
    {
        $this->miniBar = $miniBar;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMiniBar()
    {
        return $this->miniBar;
    }

    public function toArray(){
        $result = $this->__toArray();
        $result['minibar'] = $this->miniBar;
        $result['food'] = $this->food;
        return $result;
    }
    public function __construct()
    {
        $this->setObjectType('pension');
    }
}