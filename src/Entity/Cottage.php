<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CottageRepository")
 */
class Cottage extends BookingObject
{

    /**
     * @ORM\Column(type="boolean", length=254, nullable=true)
     */
    private $fireplace;

    /**
     * @ORM\Column(type="boolean", length=254, nullable=true)
     */
    private $maid;

    /**
     * @param mixed $fireplace
     * @return Cottage
     */
    public function setFireplace($fireplace)
    {
        $this->fireplace = $fireplace;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFireplace()
    {
        return $this->fireplace;
    }

    /**
     * @param mixed $maid
     * @return Cottage
     */
    public function setMaid($maid)
    {
        $this->maid = $maid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMaid()
    {
        return $this->maid;
    }

    public function toArray(){
        $result = $this->__toArray();
        $result['fireplace'] = $this->fireplace;
        $result['maid'] = $this->maid;
        return $result;
    }

    public function __construct()
    {
        $this->setObjectType('cottage');
    }

}