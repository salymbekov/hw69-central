<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 */
class Booking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $objectName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $roomNumber;

    /**
     * @ORM\Column(type="date")
     */
    private $dateOfFreedom;

    /**
     * @ORM\Column(type="string")
     */
    private $tenant;

    public function getId()
    {
        return $this->id;
    }

    public function getObjectName(): ?string
    {
        return $this->objectName;
    }

    public function setObjectName(string $objectName): self
    {
        $this->objectName = $objectName;

        return $this;
    }

    public function getRoomNumber(): ?string
    {
        return $this->roomNumber;
    }

    public function setRoomNumber(string $roomNumber): self
    {
        $this->roomNumber = $roomNumber;

        return $this;
    }

    public function getDateOfFreedom(): ?\DateTimeInterface
    {
        return $this->dateOfFreedom;
    }

    public function setDateOfFreedom(\DateTimeInterface $dateOfFreedom): self
    {
        $this->dateOfFreedom = $dateOfFreedom;

        return $this;
    }

    /**
     * @param mixed $tenant
     * @return Booking
     */
    public function setTenant($tenant)
    {
        $this->tenant = $tenant;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTenant()
    {
        return $this->tenant;
    }
}
