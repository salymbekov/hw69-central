<?php

namespace App\Repository;

namespace App\Repository;
use App\Entity\BookingObject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;
/**
 * @method BookingObject|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookingObject|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookingObject[]    findAll()
 * @method BookingObject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingObjectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BookingObject::class);
    }

    public function findByParams($name, $type, $min, $max)
    {
        $qb = $this
            ->createQueryBuilder('b')
            ->select('b');
        if (!empty($type)) {
            $qb
                ->andWhere('b.objectType = :type')
                ->setParameter('type', $type);
        }
        if (!empty($name)) {
            $qb
                ->where('b.objectName LIKE :name')
                ->setParameter('name', '%' . $name . '%');
        }
        return $qb
            ->andWhere('b.price BETWEEN :min AND :max')
            ->setParameter('min', $min ?: 0)
            ->setParameter('max', $max ?: 10000000)
            ->getQuery()
            ->getResult();

    }
    public function findByName($name)
    {
        try {
            return $this->createQueryBuilder('b')
                ->select('b')
                ->where('b.objectName = :name')
                ->setParameter('name', $name)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }
    }
}
