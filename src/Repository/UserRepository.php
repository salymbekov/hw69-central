<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findOneByOrganizationName($organizationName) : ?User
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.organizationName = :organizationName')
                ->setParameter('organizationName', $organizationName)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}

//public function findByParams(string $name, string $type, int $min, int $max)
//{
//    $qb = $this
//        ->createQueryBuilder('b')
//        ->select('b');
//    if (!empty($type)) {
//        $qb
//            ->andWhere('b.type = :type')
//            ->setParameter('type', $type);
//    }
//    if (!empty($name)) {
//        $qb
//            ->where('b.name LIKE :name')
//            ->setParameter('name', '%' . $name . '%');
//    }
//    return $qb
//        ->andWhere('b.price BETWEEN :min AND :max')
//        ->setParameter('min', $min ?: 0)
//        ->setParameter('max', $max ?: 10000000)
//        ->getQuery()->getResult();
//
//}